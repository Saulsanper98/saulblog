<?php
session_start();

require_once 'config/global_conf.php';
require_once 'core/class.file.php';
require_once 'core/BaseController.php';
require_once 'core/FrontalController.func.php';

if(isset($_GET["controller"])) {
    $controllerObj = loadController($_GET["controller"]);
    runAction($controllerObj);
} else {
    $controllerObj = loadController(DEFAULT_CONTROLLER);
    runAction($controllerObj);
}