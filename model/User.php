<?php

class User extends BaseEntity {
	
	private $id, $username, $password, $email;
	
	public function __construct($adapter) {
		parent::__construct('users', $adapter);
	}
	
	public function loadUserById($id) {
		$result = $this->getById($id);
		if ($result) {
			$this->id = $result->id;
			$this->username = $result->username;
			$this->password = $result->password;
			$this->email = $result->email;
		}
		return $result;
	}
}