<?php

class UsersModel extends BaseModel {
	
	private $table;
	
	public function __construct($adapter) {
		$this->table = 'users';
		parent::__construct($this->table, $adapter);
	}
}