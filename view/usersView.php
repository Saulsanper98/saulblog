<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<title>Usuarios</title>
</head>
<body>
	<form action="<?php echo ($editMode) ? $viewHelper->url('Users', 'updateUser') . '&id=' . $currentUser->id : $viewHelper->url('Users', 'createUser'); ?>" method="post">
		<input type="text" placeholder="Nombre de Usuario:" name="username" <?php echo ($editMode) ? 'value="' . $currentUser->username . '"' : ''; ?>/><br />
		<input type="email" placeholder="Correo Electronico:" name="email" <?php echo ($editMode) ? 'value="' . $currentUser->email . '"' : ''; ?>/><br />
		<input type="password" placeholder="Contraseña:" name="password"/><br />
		<input type="submit" value="<?php echo ($editMode) ? 'Guardar' : 'Crear Usuario'; ?>"/>
	</form>
	<br />
	<table>
		<tr>
			<th>ID</th>
			<th>Nombre de Usuario</th>
			<th>Correo electronico</th>
			<th>Opciones</th>
		</tr>
		<?php
		if ($allUsers) {
			foreach ($allUsers as $user) {
				print '<tr><td>' . $user->id . '</td>
					<td>' . $user->username . '</td>
					<td>' . $user->email . '</td>
					<td>
						<a href="' . $viewHelper->url('Users', 'editUser') . '&id=' . $user->id . '">Editar</a>
						<a href="' . $viewHelper->url('Users', 'deleteUser') . '&id=' . $user->id . '">Eliminar</a>
					</td></tr>';
			}
		}
		?>
	</table>
</body>
</html>