<?php

class UsersController extends BaseController {
	
	private $usersModel;
	
	public function __construct() {
		parent::__construct();
		$this->usersModel = new UsersModel($this->adapter);
	}
	
	public function index() {
		$result = $this->usersModel->getAll();
		$this->view('users', array(
			'allUsers' => $result,
			'editMode' => false
		));
	}
	
	public function createUser() {
		if ($this->checkPOST(array('username', 'password', 'email'))) {
			$this->usersModel->insert(array(
				'username' => $_POST['username'],
				'password' => $_POST['password'],
				'email' => $_POST['email']
			));
			$this->redirect('Users', 'index');
		}
	}
	
	public function deleteUser() {
		if (isset($_GET['id'])) {
			$this->usersModel->deleteById($_GET['id']);
		}
		$this->redirect('Users', 'index');
	}
	
	public function editUser() {
		if(isset($_GET['id'])) {
			$this->view("users", array(
				"allUsers" => $this->usersModel->getAll(),
				"editMode" => true,
				"currentUser" => $this->usersModel->getById($_GET['id'])
			));
		}
	}
	
	public function updateUser() {
		if($this->checkPOST(array("username", "password", "email")) && isset($_GET['id'])) {
			$this->usersModel->updateById($_GET['id'], array(
				'username' => $_POST['username'],
				'password' => $_POST['password'],
				'email' => $_POST['email']
			));
		}
		$this->redirect('Users', 'index');
	}
}