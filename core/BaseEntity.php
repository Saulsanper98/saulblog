<?php

class BaseEntity {
    private $table, $db, $connect;
    
    public function __construct($table, $adapter) {
        $this->table = (string)$table;
        $this->connect = null;
        $this->db = $adapter;
//        require_once 'Connect.php';
//        $this->connect = new Connect();
//        $this->db = $this->connect->connection();
    }
    
    public function getConnect() {
        return $this->connect;
    }
    
    public function db() {
        return $this->db;
    }
    
    public function getAll() {
        $query = $this->db->query("SELECT * FROM $this->table ORDER BY `id` DESC");
        if(!$query) {
            return $query;
        }
        while ($row = $query->fetch_object()) {
            $resultSet[] = $row;
        }
        if(isset($resultSet)) {
            return $resultSet;            
        }
    }
    
    public function getById($id) {
        $query = $this->db->query("SELECT * FROM $this->table WHERE `id` = '$id'");
        while ($row = $query->fetch_object()) {
            $resultSet = $row;
        }
        return (isset($resultSet)) ? $resultSet : false;
    }
    
    public function getBy($col, $val) {
        $query = $this->db->query("SELECT * FROM $this->table WHERE `$col` = '$val'");
        if($query) {
            while ($row = $query->fetch_object()) {
                $resultSet[] = $row;
            }
        }
        return (isset($resultSet)) ? $resultSet : false;
    }
    
    public function deleteById($id) {
        return $this->db->query("DELETE FROM $this->table WHERE `id` = '$id'");
    }
    
    public function deleteBy($col, $val) {
        return $this->db->query("DELETE FROM $this->table WHERE `$col` = '$val'");
    }
    
    public function updateById($id, $data) {
        if(sizeof($data) == 0) {
            return false;
        }
        $query = "UPDATE $this->table SET ";
        $iteration = 0;
        foreach($data as $id_assoc => $value) {
            $iteration++;
            if($iteration == sizeof($data)) {
                //LAST ITERATION
                $query .= "`$id_assoc` = '$value' ";
            } else {
                $query .= "`$id_assoc` = '$value', ";
            }            
        }
        $query .= "WHERE `id` = '$id'";
        return $this->db->query($query);
    }
    
    public function updateBy($col, $val, $data) {
        if(sizeof($data) == 0) {
            return false;
        }
        $query = "UPDATE $this->table SET ";
        $iteration = 0;
        foreach($data as $id_assoc => $value) {
            $iteration++;
            if($iteration == sizeof($data)) {
                //LAST ITERATION
                $query .= "`$id_assoc` = '$value' ";
            } else {
                $query .= "`$id_assoc` = '$value', ";
            }            
        }
        $query .= "WHERE `$col` = '$val'";
        return $this->db->query($query);
    }
    
    public function insert($data) {
        if(sizeof($data) == 0) {
            return "DATA EMPTY";
        }
        $query = "INSERT INTO $this->table(";
        $values = "VALUES (";
        $iteration = 0;
        foreach($data as $id_assoc => $value) {
            $iteration++;
            if($iteration == sizeof($data)) {
                //LAST ITERATION
                $query .= "`$id_assoc`) ";
                $values .= "'$value')";
            } else {
                $query .= "`$id_assoc`, ";
                $values .= "'$value', ";
            }            
        }
        $final = $query . $values;
        return $this->db->query($final);
    }
}

