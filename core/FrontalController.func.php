<?php

function loadController($controller) {
    $cont = ucwords($controller) . "Controller";
    $fileController = "controller/$cont.php";
    
    if(!is_file($fileController)) {
        $cont = ucwords(DEFAULT_CONTROLLER) . "Controller";
        $fileController = "controller/" . ucwords(DEFAULT_CONTROLLER) . "Controller.php";
    }
    
    require_once $fileController;
    $controllerObj = new $cont();
    return $controllerObj;
}

function loadAction($controllerObj, $action) {
    $controllerObj->$action();
}

function runAction($controllerObj) {
    if(isset($_GET["action"]) && method_exists($controllerObj, $_GET["action"])) {
        loadAction($controllerObj, $_GET["action"]);
    } else {
        loadAction($controllerObj, DEFAULT_ACTION);
    }
}
