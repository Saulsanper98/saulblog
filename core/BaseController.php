<?php

class BaseController {

    public $connect, $adapter;

    public function __construct() {
        require_once 'Connect.php';
        require_once 'BaseEntity.php';
        require_once 'BaseModel.php';
        require_once 'model/UsersModel.php';
        require_once 'model/User.php';

        $this->connect = new Connect();
        $this->adapter = $this->connect->connection();
    }
    
    public function view($view, $data) {
        foreach ($data as $id_assoc => $val) {
            ${$id_assoc} = $val;
        }
        
        require_once 'ViewHelper.php';
        $viewHelper = new ViewHelper();

        require_once 'view/' . $view . "View.php";
    }
    
    public function redirect($controller=DEFAULT_CONTROLLER, $action=DEFAULT_ACTION) {
        header("Location:index.php?controller=$controller&action=$action");
    }

    public function hasPermissions() {
        if(isset($_SESSION["isAdmin"])) {
            if(!(bool)$_SESSION["isAdmin"]) {
                return false;
            }
        }
        return true;
    }

    public function isLoggedIn() {
        if(isset($_SESSION["user"], $_SESSION["idUser"], $_SESSION["isAdmin"])) {
            return true;
        }
        return false;
    }

    public function authAction() {
        if(!$this->isLoggedIn()) {
            $this->redirect("Users", "login");
            return false;
        }
        if(!$this->hasPermissions()) {
            return false;
        }
        return true;
    }

    public function error($errCode) {
        switch($errCode) {
            case "BADLOGIN":
                $msg = "Usuario o contraseña incorrectos.";
                break;
            case "":

            default:
                $msg = "Ha ocurrido un error desconocido, contacte con un administrador.";
                break;
        }
        return $msg;
    }

    public function checkPOST($fields) {
        $error = true;
        foreach($fields as $field) {
            if(!isset($_POST[$field]) || empty($_POST[$field])) {
                $error = false;
                break;
            }
        }
        return $error;
    }
}

