<?php

class BaseModel extends BaseEntity {
    private $table;
    
    public function __construct($table, $adapter) {
        $this->table = (string)$table;
        parent::__construct($table, $adapter);
    }
    
    public function runSql($query){
        $out=$this->db()->query($query);
        if($out == true){
            if($out->num_rows > 1){
                while($row = $out->fetch_object()) {
                   $resultSet[] = $row;
                }
            }elseif($out->num_rows==1){
                if($row = $out->fetch_object()) {
                    $resultSet = $row;
                }
            }else{
                $resultSet = true;
            }
        }else{
            $resultSet = false;
        }
         
        return $resultSet;
    }
}

