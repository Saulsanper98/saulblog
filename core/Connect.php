<?php

class Connect {
    private $host, $port, $db, $user, $pass;
    
    public function __construct() {
        $db_conf = require_once 'config/db_conf.php';
        $this->host = $db_conf["host"];
        $this->port = $db_conf["port"];
        $this->db = $db_conf["db"];
        $this->user = $db_conf["user"];
        $this->pass = $db_conf["pass"];
    }
    
    public function connection() {
        return new mysqli($this->host, $this->user, $this->pass, $this->db, $this->port);
    }
}

